   ( function()
        {
            // execute first time
            //
            convertMatrix();

            function convertMatrix()
            {

                var minWidth = 767;

                var applyResponsiveness = document.documentElement.clientWidth < minWidth;


                var pageRows = document.querySelectorAll( ".nqodd, .nqeven" );

                var idx = 0
                ,   el
                ;
                while ( el = pageRows[ idx++ ] )
                {

                    var table = el.querySelector( "table" );


                    // if table has another table, we don't it is not a matrix
                    //
                    if ( table.querySelector( "table" ) )
                    {
                        continue;
                    }


                    // now get all rows of the table
                    //
                    var rows = [].slice.call( table.querySelectorAll( "tr" ) );

                    // isolate first row: the header
                    //
                    var header = rows.shift();


                    if ( applyResponsiveness )
                    {
                        header.setAttribute( "style", "display:none" );
                    }
                    else
                    {
                        header.removeAttribute( "style" );
                    }

                    header = [].slice.call( header.querySelectorAll( "td.nqoption" ) );


                    // loop the remaining rows
                    //
                    var row
                    ,   x=0
                    ;
                    while ( row = rows[ x++ ] )
                    {
                        var cells = [].slice.call( row.querySelectorAll( "td" ) );

                        // remove first
                        //
                        cells.shift();
                        var cells
                        ,   ci=0
                        ;
                        while( cell = cells[ ci++ ])
                        {

                            if ( cell.querySelector( ".lblMobileMatrix" ) )
                            {
                                cell.removeChild( cell.querySelector( ".lblMobileMatrix" ) );
                                cell.setAttribute( "width", cell.getAttribute( "data-width" ) );
                                cell.removeAttribute( "data-width" );
                            }
                            if ( applyResponsiveness )
                            {
                                cell.setAttribute( "data-width", cell.getAttribute( "width" ) );
                                cell.removeAttribute( "width" );

                                var label           = document.createElement( "span" );
                                label.className     = "lblMobileMatrix";

                                label.innerText     = header[ ci-1 ].innerText.replace( /(\r\n)|(\n)|(\r)/gi, "" );
                                cell.appendChild( label );
                            }

                        };
                    };
                };
            }

            // bind function to resize event
            //
            window.onresize = convertMatrix;

        }());
