# README #

### How it works ###

- This template enables responsive layouts in NETQ surveys
- Includes responsive matrix questions (look like tables on desktops and tablets, turn into a scrollable list of multiple choice questions on mobile phones

### Using the template ###

- Upload the code to a folder on the web
- Add the template folder into NETQ using Library - Templates - Add
- Type in the folder's URL in the URL field and click 'OK'
- Refresh the new template in NETQ to make sure all template files are loaded

You can now use this template to create responsive surveys in NETQ
